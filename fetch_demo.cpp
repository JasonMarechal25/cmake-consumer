
#include <string>
#include "fetch_demo.h"
#include "testapi/Collaborator.h"
#include "testapi/Front.h"
//#include "ortools/linear_solver/linear_solver.h"

void libmain::foo() const noexcept {
    using namespace std::string_literals;
    API::Collaborator c;
    API::Front f("plop"s, 4);
    f.printIt(c);
    //operations_research::MPSolver solver("foo", operations_research::MPSolver::GLOP_LINEAR_PROGRAMMING);
}
