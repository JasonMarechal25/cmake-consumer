install(EXPORT FetchDemoTargets
        FILE FetchDemoTargets.cmake
        NAMESPACE FetchDemo::
        DESTINATION lib/cmake/FetchDemo
)

configure_file(FetchDemoConfig.cmake.in FetchDemoConfig.cmake @ONLY)
install(FILES "${CMAKE_CURRENT_BINARY_DIR}/FetchDemoConfig.cmake"
        "${CMAKE_CURRENT_BINARY_DIR}/FetchDemoConfigVersion.cmake"
        DESTINATION lib/cmake/FetchDemo
)